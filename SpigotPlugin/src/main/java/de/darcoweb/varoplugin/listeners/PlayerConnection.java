package de.darcoweb.varoplugin.listeners;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.GameState;
import de.darcoweb.varoplugin.utilities.Message;
import de.darcoweb.varoplugin.utilities.manager.BanManager;
import de.darcoweb.varoplugin.utilities.manager.TimerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerConnection implements Listener {

    private final VaroPlugin plugin = VaroPlugin.getInstance();

    @EventHandler
    public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent e) {
        if (plugin.isState(GameState.RUNNING)) {
            if (VaroPlugin.getInstance().getTeamManager()
                    .getPlayersTeam(Bukkit.getOfflinePlayer(e.getUniqueId())) == null) {
                e.disallow(Result.KICK_OTHER, ChatColor.RED + Message.join.NOT_PARTICIPATING());
            } else {
                BanManager banManager = VaroPlugin.getInstance().getBanManager();
                OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(e.getUniqueId());
                if (banManager.isBanned(offlinePlayer)) {
                    if (banManager.isBannedForever(offlinePlayer)) {
                        e.disallow(Result.KICK_OTHER, ChatColor.RED + Message.join.ELIMINATED());
                    } else {
                        e.disallow(Result.KICK_OTHER, ChatColor.RED + Message.join.NO_TIME_LEFT());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);

        Player p = e.getPlayer();
        plugin.updateGameMode(p);
        plugin.updateName(p);

        if (plugin.getState().equals(GameState.RUNNING)) {
            TimerManager timerManager = VaroPlugin.getInstance().getTimerManager();
            if (timerManager.isTracked(p)) {
                int secondsLeft = timerManager.getValueForPlayer(p).getTimeLeft();
                Chat.info.serverBroadcast(ChatColor.YELLOW + p.getName() + Chat.info.color()
                        + " hat das Spiel wieder betreten und darf noch " + ChatColor.BOLD + secondsLeft / 60
                        + ":" + String.format("%02d", secondsLeft % 60) + ChatColor.RESET + "" + Chat.info.color()
                        + " Minuten spielen!");
            } else {
                timerManager.newTrackedPlayer(plugin, p, true);
            }
        }

        VaroPlugin.getInstance().getScoreboardManager().registerBoard(p.getUniqueId());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);
        Player p = e.getPlayer();
        VaroPlugin.getInstance().getScoreboardManager().unregister(p.getUniqueId());
        Chat.info.serverBroadcast(ChatColor.YELLOW + p.getName() + Chat.info.color() + " hat das Spiel verlassen"
                + (VaroPlugin.getInstance().getTimerManager().isTracked(p) ? ", aber noch Zeit übrig" : "") + "!");
    }
}
