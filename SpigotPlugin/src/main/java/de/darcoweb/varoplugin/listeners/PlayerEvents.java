package de.darcoweb.varoplugin.listeners;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.utilities.GameState;
import de.darcoweb.varoplugin.utilities.Team;
import de.darcoweb.varoplugin.utilities.manager.TimerManager;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import static java.lang.Math.floor;

public class PlayerEvents implements Listener {

    VaroPlugin plugin = VaroPlugin.getInstance();

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        if (isInvincible(p)) {
            Location from = e.getFrom();
            double xFrom = floor(from.getX()), yFrom = floor(from
                    .getY()), zFrom = floor(from.getZ());
            Location to = e.getTo();
            double xTo = floor(to.getX()), yTo = floor(to.getY()), zTo = floor(to.getZ());
            if (xFrom != xTo || yFrom != yTo || zFrom != zTo) {
                p.teleport(new Location(p.getWorld(), xFrom + 0.5, yFrom,
                        zFrom + 0.5, from.getYaw(), from.getPitch()));
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (isInvincible(p))
            e.setCancelled(true);
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player) {
            if (isInvincible((Player) e.getDamager()))
                e.setCancelled(true);
            else if (e.getEntity() instanceof Player) {
                Team team = plugin.getTeamManager().getPlayersTeam((Player) e.getEntity());
                if (!plugin.getConfig().getBoolean("friendly_fire")
                        && team != null) {
                    if (team.hasPlayer(e.getDamager().getUniqueId()))
                        e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            if (!plugin.isState(GameState.RUNNING))
                e.setCancelled(true);
            else if (isInvincible((Player) e.getEntity()))
                e.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityTarget(EntityTargetEvent e) {
        if (e.getTarget() instanceof Player) {
            Player p = (Player) e.getTarget();
            if (isInvincible(p)) {
                e.setCancelled(true);
            }
        }
    }

    private boolean isInvincible(Player p) {
        if (plugin.isState(GameState.RUNNING)) {
            TimerManager tm = plugin.getTimerManager();
            if (tm.isTracked(p)) {
                if (tm.getValueForPlayer(p).isInvincible())
                    return true;
            }
        }
        return false;
    }
}
