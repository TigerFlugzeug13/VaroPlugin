package de.darcoweb.varoplugin.utilities;

import lombok.Data;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;

@Data
public class TimerTuple {

    // TODO Reduce this to saving the UUID only..
    private OfflinePlayer player;
    private int timeLeft;
    private int invincibleLeft;

    /**
     * Constructs a fresh timer tuple from values read from the plugin's config.yml.
     *
     * @param player       The player to be associated with this tuple
     * @param doInvincibility If the player should be invincible for a given amount of time.
     */
    public TimerTuple(OfflinePlayer player, FileConfiguration config, boolean doInvincibility) {
        this.player = player;
        this.timeLeft = 60 * config.getInt("playing_each_day.max_daily_time");
        this.invincibleLeft = doInvincibility ? config.getInt("playing_each_day.join_invincibility") : -1;
    }

    /**
     * Constructs a simple timer tuple from the given values.
     * Pass invincibleLeft in as <code>-1</code> for no invincibility.
     *
     * @param offlinePlayer  The player to be associated with this tuple
     * @param timeLeft       The time the player has left to play
     * @param invincibleLeft The time the player will still be invincible
     */
    public TimerTuple(OfflinePlayer offlinePlayer, int timeLeft, int invincibleLeft) {
        this.player = offlinePlayer;
        this.timeLeft = timeLeft;
        this.invincibleLeft = invincibleLeft;
    }

    /**
     * @return Whether the player has invincibility time left
     */
    public boolean isInvincible() {
        return invincibleLeft >= 0;
    }
}
