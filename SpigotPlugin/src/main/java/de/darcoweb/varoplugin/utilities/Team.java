package de.darcoweb.varoplugin.utilities;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.utilities.manager.BanManager;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
public class Team {

    private final String name;

    private ChatColor color = ChatColor.WHITE;
    private Location chestLocation;
    private boolean alive;

    private Set<UUID> uuids = new HashSet<>();


    public void addPlayer(UUID uuid) {
        this.uuids.add(uuid);
    }

    public void removePlayer(UUID uuid) {
        this.uuids.remove(uuid);
    }

    public boolean hasPlayer(UUID uuid) {
        return this.uuids.contains(uuid);
    }

    public Set<OfflinePlayer> getPlayers() {
        return this.uuids.stream().map(Bukkit::getOfflinePlayer).collect(Collectors.toSet());
    }

    public int getSize() {
        return this.uuids.size();
    }

    public boolean isAlive() {
        updateAlive();
        return this.alive;
    }

    public void updateAlive() {
        setAlive(false);
        if (uuids.isEmpty()) return;

        BanManager banManager = VaroPlugin.getInstance().getBanManager();
        if (uuids.stream().anyMatch(
                uuid -> !banManager.isOutOfGame(
                        Bukkit.getOfflinePlayer(uuid))))
            setAlive(true);
    }
}
