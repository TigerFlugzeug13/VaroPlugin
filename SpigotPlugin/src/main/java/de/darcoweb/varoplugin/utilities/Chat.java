package de.darcoweb.varoplugin.utilities;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Static implementation of a ChatUtilities class with different message types that yield differently formatted text.
 * Can send individual messages and several broadcasts, of course in different colors associated with the types.
 *
 * @author 1Darco1
 */
public abstract class Chat {

    public static final ChatErr err = new ChatErr();
    public static final ChatInfo info = new ChatInfo();
    public static final ChatSuccess success = new ChatSuccess();
    public static final ChatCustom custom = new ChatCustom();

    private static final ChatColor pluginColor = ChatColor.BLUE;
    private static final String pluginPrefix = pluginColor + "VaroPlugin: " + ChatColor.RESET;

    private Chat() {
    }

    public void serverBroadcast(String msg) {
        Bukkit.broadcastMessage(tag() + colorify(msg));
    }

    public void opBroadcast(String msg) {
        Bukkit.getConsoleSender().sendMessage(tag() + colorify(msg));
        Bukkit.getOnlinePlayers().stream().filter(Player::isOp)
                .forEach(player -> player.sendMessage(tag() + colorify(msg)));
    }

    public void broadcast(String msg) {
        Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage(tag() + colorify(msg)));
    }

    public void send(CommandSender sender, String msg) {
        sender.sendMessage(tag() + colorify(msg));
    }

    public void sendRaw(CommandSender sender, String msg) {
        sender.sendMessage(colorify(msg));
    }

    private String colorify(String msg) {
        return msg.replaceAll("§q", "§r§" + color().getChar());
    }

    public String tag() {
        return pluginPrefix + color();
    }

    public abstract ChatColor color();


    public static class ChatErr extends Chat {

        private ChatErr() {
        }

        @Override
        public ChatColor color() {
            return ChatColor.RED;
        }
    }

    public static class ChatInfo extends Chat {

        private ChatInfo() {
        }

        @Override
        public ChatColor color() {
            return ChatColor.AQUA;
        }

    }

    public static class ChatSuccess extends Chat {

        private ChatSuccess() {
        }

        @Override
        public ChatColor color() {
            return ChatColor.GREEN;
        }
    }

    public static class ChatCustom extends Chat {

        private ChatCustom() {
        }

        @Override
        public ChatColor color() {
            return ChatColor.RESET;
        }
    }
}
