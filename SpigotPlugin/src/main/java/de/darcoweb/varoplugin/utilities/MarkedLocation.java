package de.darcoweb.varoplugin.utilities;

import lombok.*;
import lombok.experimental.Accessors;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represents a location marked by an {@link org.bukkit.entity.ArmorStand}.
 *
 * @author 1Darco1
 */
@Data
public class MarkedLocation {

    public static final String SEPARATOR = ":";

    /**
     * Creates a new MarkedLocation object from a string created with {@link #toSerializedString()}.
     *
     * @param serializedString The serialized string containing all the information
     * @return A new MarkedLocations object with all fields set
     */
    public static MarkedLocation fromSerializedString(String serializedString) {
        String[] params = serializedString.split(SEPARATOR);

        UUID uuid = UUID.fromString(params[0]);
        String world = params[1];

        List<Double> numbers = Stream.of(params).skip(2).map(Double::parseDouble).collect(Collectors.toList());
        Location location = new Location(Bukkit.getWorld(world), numbers.get(0), numbers.get(1), numbers.get(2),
                numbers.get(3).floatValue(), numbers.get(3).floatValue());

        return new MarkedLocation(location, uuid);
    }

    protected final Location location;
    protected final UUID armorStandUuid;

    public MarkedLocation(@NonNull Location location, @NonNull UUID uuid) {
        this.location = location;
        this.armorStandUuid = uuid;
    }

    public String toSerializedString() {
        return armorStandUuid.toString() + SEPARATOR +
                location.getWorld().getName() + SEPARATOR +
                location.getX() + SEPARATOR +
                location.getY() + SEPARATOR +
                location.getZ() + SEPARATOR +
                location.getYaw() + SEPARATOR +
                location.getPitch();
    }

    /**
     * This class should be used as a singleton, so there should only be one instance created per needed variable location.
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    public static class MutableMarkedLocation {

        public static MutableMarkedLocation fromSerializedString(String serializedString) {
            String[] params = serializedString.split(SEPARATOR);

            UUID uuid = UUID.fromString(params[0]);
            String world = params[1];

            List<Double> numbers = Stream.of(params).skip(2).map(Double::parseDouble).collect(Collectors.toList());
            Location location = new Location(Bukkit.getWorld(world), numbers.get(0), numbers.get(1), numbers.get(2),
                    numbers.get(3).floatValue(), numbers.get(3).floatValue());

            return new MutableMarkedLocation().setLocation(location).setArmorStandUuid(uuid);
        }

        protected Location location;
        protected UUID armorStandUuid;

        public String toSerializedString() {
            return armorStandUuid.toString() + SEPARATOR +
                    location.getWorld().getName() + SEPARATOR +
                    location.getX() + SEPARATOR +
                    location.getY() + SEPARATOR +
                    location.getZ() + SEPARATOR +
                    location.getYaw() + SEPARATOR +
                    location.getPitch();
        }

        public MarkedLocation immutableCopy() throws IllegalStateException {
            if (!isLocationSet() || !isArmorStandUuidSet()) throw new IllegalStateException("All fields must be set!");

            return new MarkedLocation(location, armorStandUuid);
        }

        public boolean isLocationSet() { return location != null; }
        public boolean isArmorStandUuidSet() { return armorStandUuid != null; }
    }
}
