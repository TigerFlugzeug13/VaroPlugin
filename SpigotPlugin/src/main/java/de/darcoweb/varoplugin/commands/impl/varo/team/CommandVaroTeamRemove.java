package de.darcoweb.varoplugin.commands.impl.varo.team;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import de.darcoweb.varoplugin.utilities.Team;
import de.darcoweb.varoplugin.utilities.manager.TeamManager;
import org.bukkit.command.CommandSender;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroTeamRemove extends SubCommand<VaroPlugin> {

    public CommandVaroTeamRemove(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "remove", "Removes a team", "Team");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 1) {
            Chat.err.send(sender, Message.general.INVALID_ARGUMENTS());
            return;
        }

        TeamManager teamManager = rootCommand.getPlugin().getTeamManager();
        Team team = teamManager.getTeam(args[0]);
        if (team != null) {
            teamManager.remove(team);
            Chat.success.send(sender, Message.team.WAS_DELETED(args[0]));
        } else
            Chat.err.send(sender, Message.team.NOT_EXISTS());
    }
}
