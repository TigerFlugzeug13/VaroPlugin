package de.darcoweb.varoplugin.event;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.inventory.BrewerInventory;
import org.bukkit.plugin.PluginManager;

public class BrewEventListener implements Listener {

	@EventHandler
	public void onBrew(BrewEvent e) {
		BrewerInventory inv = e.getContents();
		Material ingredient = inv.getIngredient().getType();

		BrewEvent customizedEvent = null;
		if (ingredient.equals(Material.REDSTONE)) {
			customizedEvent = new ExtendBrewEvent(e.getBlock(), e.getContents());
		} else if (ingredient.equals(Material.GLOWSTONE_DUST)) {
			customizedEvent = new StrengthenBrewEvent(e.getBlock(), e.getContents());
		} else if (ingredient.equals(Material.SULPHUR)) {
			customizedEvent = new ThrowableBrewEvent(e.getBlock(), e.getContents());
		}

		if (customizedEvent != null) {
			PluginManager pluginManager = Bukkit.getPluginManager();
			pluginManager.callEvent(customizedEvent);

			if (customizedEvent.isCancelled())
				e.setCancelled(true);
		}
	}
}
